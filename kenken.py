import sys
import time
import ast
from csp import *


class Cage:
    """
    operation       'add' | 'sub' | 'mult' | 'div'
    cells           list cage's cells
    values          values assigned (only used by division and subtraction for
                    the remove_assignment method)
    current_score   current score from all the assignments
    """
    def __init__(self, target_number, operation, cells):
        self.cells_assigned = 0
        self.target_number = target_number
        self.operation = operation
        self.cells = cells
        # values of the cells (only for division and subtraction)
        self.values = []

        if operation == 'add' or operation == 'sub' or operation == "''":
            self.current_score = 0
        else:   # 'div' or 'mult'
            self.current_score = 1

    def add_assignment(self, val):
        if self.operation == 'add':
            self.current_score += val
        elif self.operation == 'mult':
            self.current_score *= val
        elif self.operation == 'sub':
            self.current_score = max(self.current_score, val) - min(self.current_score, val)
            self.values.append(val)
        elif self.operation == 'div':
            self.current_score = max(self.current_score, val) // min(self.current_score, val)
            self.values.append(val)
        self.cells_assigned += 1

    def remove_assignment(self, val):
        if self.operation == 'add':
            self.current_score -= val
        elif self.operation == 'mult':
            self.current_score //= val
        elif self.operation == 'sub':
            self.values.remove(val)
            if len(self.values) > 0:
                self.current_score = self.values[0]
            else:
                self.current_score = 0
        elif self.operation == 'div':
            self.values.remove(val)
            if len(self.values) > 0:
                self.current_score = self.values[0]
            else:
                self.current_score = 1
        self.cells_assigned -= 1

    def constraint(self, new_val):
        if self.operation == 'add':
            if self.cells_assigned + 1 < len(self.cells):
                return self.current_score + new_val < self.target_number
            else:
                return self.current_score + new_val == self.target_number
        elif self.operation == 'mult':
            if self.cells_assigned + 1 < len(self.cells):
                return self.current_score * new_val <= self.target_number
            else:
                return self.current_score * new_val == self.target_number
        elif self.operation == 'div':
            if self.cells_assigned + 1 < len(self.cells):
                return True
            else:
                return max(self.current_score, new_val) // min(self.current_score, new_val)\
                       == self.target_number and max(self.current_score, new_val)\
                                                 % min(self.current_score, new_val) == 0
        elif self.operation == 'sub':
            if self.cells_assigned + 1 < len(self.cells):
                return True
            else:
                return max(self.current_score, new_val) - min(self.current_score, new_val)\
                       == self.target_number
        else:
            return True

# ______________________________________________________________________________
# Kenken puzzle

class KenKen(CSP):
    """A KenKen puzzle.
    Grids range in size from 3×3 to 9×9. Additionally, KenKen grids are
    divided into heavily outlined groups of cells –– often called “cages”
    –– and the numbers in the cells of each cage must produce a certain
    “target” number when combined using a specified mathematical operation
    (either addition, subtraction, multiplication or division). For example,
    a linear three-cell cage specifying addition and a target number of 6 in
    a 4×4 puzzle must be satisfied with the digits 1, 2, and 3. Digits may be
    repeated within a cage, as long as they are not in the same row or column.
    No operation is relevant for a single-cell cage: placing the "target" in
    the cell is the only possibility (thus being a "free space").

    puzzle      string
    format:

    puzzle size
    <list of tuples> <mathematical operation> <target number>
    <list of tuples> '' <target number>
    ...

    mathematical operation = add | sub | mult | div
    EXAMPLE:
    3
    [(0,0),(1,0),(1,1)] add 5
    [(0,1)] '' 2
    [(0,2),(1,2)] add 5
    [(2,0),(2,1)] add 5
    [(2,2)] '' 1

    variables   A list of variables; each is atomic (e.g. int or string).
    domains     A dict of {var:[possible_value, ...]} entries.
    neighbors   A dict of {var:[var,...]} that for each variable lists
                the other variables that participate in constraints.
    variables_cages    A dict of {var:[cage_id]} entries
    cages_dict         A dict of {cage_id:[cage]} entries
    constraints_checks_count    tracks the the number of constraints checks
    size                size of the puzzle (example: 3 for 3x3 puzzles)
    """

    def __init__(self, puzzle):

        puzzle_lines = str(puzzle).splitlines()
        self.size = int(puzzle_lines[0])
        variables = [str(y) + str(x) for x in range(self.size) for y in range(self.size)]
        # key: variable | value: cage id
        self.variables_cages = {}
        self.cages_dict = {}
        self.vars_assignment = {}
        self.constraints_checks_count = 0

        # set the neighbors for each variable (same row and same column elements)
        neighbors = {}
        for var in variables:
            neighbors_set = set(str(y) + str(var[1]) for y in range(self.size))
            for x in range(self.size):
                neighbors_set.add(str(var[0]) + str(x))
            # remove var from the neighbors
            neighbors_set.remove(var)
            neighbors[var] = neighbors_set

        domains = {}
        for var in variables:
            domains[var] = list(range(1, self.size + 1))

        # first line is puzzle's size
        for i in range(1, len(puzzle_lines)):
            args = puzzle_lines[i].split(' ')
            cells_list = ast.literal_eval(args[0])
            # set the domain for single cell cages
            if args[1] == "''":
                cell = cells_list[0]
                index = str(cell[0]) + str(cell[1])
                domains[index] = [int(args[2])]

            # add variables in cages
            lst = []
            for cell in cells_list:
                var_string = str(cell[0]) + str(cell[1])
                self.variables_cages[var_string] = i
                lst.append(var_string)

            new_cage = Cage(int(args[2]), args[1], lst)
            self.cages_dict[i] = new_cage

        CSP.__init__(self, variables, domains, neighbors, self.values_constraint)

    def assign(self, var, val, assignment):
        "Add {var: val} to assignment; Discard the old value if any."
        old_val = assignment.get(var, None)
        if val != old_val:
            cage_id = self.variables_cages[var]
            cage = self.cages_dict[cage_id]
            if old_val is not None:
                cage.remove_assignment(old_val)
            cage.add_assignment(val)
            self.vars_assignment[var] = val
            CSP.assign(self, var, val, assignment)

    def unassign(self, var, assignment):
        "Remove {var: val} from assignment."
        if var in assignment:
            cage_id = self.variables_cages[var]
            cage = self.cages_dict[cage_id]
            cage.remove_assignment(assignment[var])
            del self.vars_assignment[var]
        CSP.unassign(self, var, assignment)

    def values_constraint(self, A, a, B, b):
        def check_constraint(var, val):
            cage_id = self.variables_cages[var]
            cage = self.cages_dict[cage_id]
            if var in self.vars_assignment.keys():
                cage.remove_assignment(self.vars_assignment[var])
            res = cage.constraint(val)
            if var in self.vars_assignment.keys():
                cage.add_assignment(self.vars_assignment[var])
            return res

        self.constraints_checks_count += 1
        if a != b:
            res = True
            if A not in self.vars_assignment.keys():
                res = check_constraint(A, a)
            return res and check_constraint(B, b)
        else:
            return False

    def nconflicts(self, var, val, assignment):
        "Return the number of conflicts var=val has with other variables."
        # Subclasses may implement this more efficiently
        def conflict(var2):
            return (var2 in assignment and
                    not self.constraints(var2, assignment[var2], var, val))
        return count(conflict(v) for v in self.neighbors[var])

    def get_total_assignments(self):
        return self.nassigns

    def get_total_constraints_count(self):
        return self.constraints_checks_count

    def simple_display(self, assignment):
        for y in range(self.size):
            for x in range(self.size):
                var = str(y) + str(x)
                print(assignment[var], end='', sep='')
                if x != self.size - 1:
                    print(" ", end='')
            print("", sep='')

    def display(self, assignment):
        print("\nSolution:")
        print(" ", "-" * self.size * 2)
        for y in range(self.size):
            print("| ", end='')
            for x in range(self.size):
                var = str(y) + str(x)
                print(assignment[var], end='', sep='')
                if x != self.size - 1:
                    print(" ", end='')
            print(" |", sep='')
        print(" ", "-" * self.size * 2, "\n")

# _______________________________________________________________________________
# Main

if __name__ == "__main__":

    if len(sys.argv) < 3:
        print("usage: python kenken.py input_file algorithm (bt | bt-mrv "
                                            "| fc | fc-mrv | mac | min_con)")
        exit()

    file_name = sys.argv[1]
    try:
        file = open(file_name, 'r')
    except:
        print("File not found!")
        exit()

    new_puzzle = file.read()
    file.close()
    result = None
    k = KenKen(new_puzzle)
    start_time = time.time()
    if sys.argv[2] == "bt":
        result = backtracking_search(k)
    elif sys.argv[2] == "bt-mrv":
        result = backtracking_search(k, select_unassigned_variable=mrv)
    elif sys.argv[2] == "fc":
        result = backtracking_search(k, inference=forward_checking)
    elif sys.argv[2] == "fc-mrv":
        result = backtracking_search(k, select_unassigned_variable=mrv, inference=forward_checking)
    elif sys.argv[2] == "mac":
        result = backtracking_search(k, inference=mac)
    elif sys.argv[2] == "min_con":
        result = min_conflicts(k)
    else:
        print("Error: wrong algorithm!\nAvailable algorithms: bt | bt-mrv "
                                            "| fc | fc-mrv | mac | min_con")
        exit()
    end_time = time.time()

    if len(sys.argv) == 4 and sys.argv[3] == "simple":
        if result is not None:
            k.simple_display(result)
        else:
            print("\nSolution not found")
    else:
        if result is not None:
            k.display(result)
        else:
            print("\nSolution not found")
        print("Total constraints checks:", k.get_total_constraints_count())
        print("Total assignments:", k.get_total_assignments())
        print("Elapsed time:", end_time - start_time, "seconds")
        print("")
