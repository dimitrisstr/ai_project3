#! /bin/bash
executable="kenken.py"
puzzles_folder="input_files/puzzles"
solutions_folder="input_files/solutions"
puzzles_files=`ls ${puzzles_folder}`

count=0
for file in ${puzzles_files}
do
    echo "Testing ${file}"
    python ${executable} ${puzzles_folder}/${file} $1 simple > output
    solution_file=`echo ${file} | sed 's/.txt//'`
    diff output "${solutions_folder}/${solution_file}_sol.txt"
    let count=count+1
done
rm output
echo "Tested files: ${count}"
